# GHC Performance Notes

A repository to hold git notes related to the [main GHC repository](https://gitlab.haskell.org/ghc/ghc).

See [the Wiki](https://gitlab.haskell.org/ghc/ghc/wikis/building/running-tests/performance-tests) for details.